﻿#Sensitive Information Classifier
#RJ Cantrell, Sriram Dandapani, Biswadeep Sarkar

param([string]$inFile) #Must be the first statement in your script - this is a param for the overall SCRIPT

#setting some variables to be used in functions and the script itself
$defaultInFile = '..\sample-nonsensitive.txt'
$DebugPreference = 'Continue' #Continue will show the message, SilentlyContinue will not, Stop stops, Inquire asks user
$ErrorActionPreference = 'Continue'

#Powershell script execution begins here!
cls

#You would have had to had tried to install the module to your system, I guess, but...
if (Get-Module -Name PersonalInfoScorer) { 
    Write-Debug 'Found PersonalInfoScorer already imported. Removing module...'
    Remove-Module PersonalInfoScorer 
}

$modulePath = Join-Path $PSScriptRoot 'PersonalInfoScorer'
Write-Debug "Importing $modulePath"
Import-Module -Name $modulePath

if(-not($inFile)) { 
    Write-Host 'No input file specified. Using default filename' $DefaultInFile
}
$inFile = $defaultInFile

Score-Document -Filename $inFile