import datetime
import logging
import os
import subprocess
import sys
import shutil

# Options you may be interested in tweaking
is_debug = True
training_set_path = ".\\data"
classifier_type = 'MaxEnt'
classifier_name = 'CS410Final'

# the script's sesison_id determines output folder. A value that changes will write new folders.
# a static string value will overwrite previous runs. This value archives all executions
session_id = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S_%f')

# Options you should probably not tweak
mallet_path = os.path.join('bin', 'mallet', 'bin', 'mallet')  # .\bin\mallet\bin\mallet

# Buggy, so please don't change this. This makes Mallet create 10 different classifiers,
# and I never updated this script to choose the best trial for use in classifying
cross_validation = ''  # '10'  #number of folds to use in CV, or '' to skip. Please skip.


def score_document(file_path):
    """
    Score a single document by all available analyses.

    Currently, the two analyses done are to classify the document as either sensitive
    or nonsensitive via the MALLET text analysis library, and the results of bin\CheckPII.jar

    :param file_path: the path to the text file you wish to score.

    :return: a dictionary of the results of each analysis. The key is a
    descriptive term of the analysis, and the value is the result thereof.
    """

    # set up logging filename and format
    log_file = os.path.join(_output_path(), 'scorer.log')
    log_fmt = '%(asctime)s - %(levelname)-8s %(message)s'
    logging.basicConfig(filename=log_file, level=logging.DEBUG, format=log_fmt)

    # the basicConfig only writes to a file, but let's write to stderr too.
    logger = logging.getLogger()
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    logger.addHandler(console)

    _debug(f'New session begun. Scoring "{file_path}".\r\n')
    _check_requirements(file_path)

    ret_val = {}
    labels = _build_classifier()
    classify_results = _classify_file(file_path, labels)

    # classify_result returns a dictionary, so merge all its keys into the return value
    ret_val.update(classify_results)
    ret_val['pii_result'] = _check_file_for_pii(file_path)

    logger.debug('Scoring complete. Final results:')
    logger.debug(ret_val)
    return ret_val


def _check_requirements(file_path):
    """
    Verify that the files needed to perform analysis are all present.

    :param file_path: The path to the text file you wish to score.
    :return: None. If a required file is not found, a RuntimeError is raised.
    """

    _debug('Check-Requirements is verifying necessary components...')

    _debug(f' -- Looking for {file_path}')
    if not os.path.exists(file_path):
        raise RuntimeError(f'Input file "{file_path}" not found!')

    paths = [training_set_path, os.path.join('bin', 'mallet'),
             os.path.join(training_set_path, 'nonsensitive'),
             os.path.join(training_set_path, 'sensitive')]

    for p in paths:
        _debug(f' -- Looking for the data folder {os.path.abspath(p)}')
        if not os.path.exists(os.path.abspath(p)):
            err = f'Could not find path {os.path.abspath(p)}!'
            _debug(err)
            raise RuntimeError(err)

        _debug(f' -- Checking whether {os.path.abspath(p)} is empty')
        if not os.listdir(os.path.abspath(p)):
            err = f'{p} exists but ontains no files for training a classifier!'
            _debug(err)
            raise RuntimeError(err)

    _debug(' -- Looking for the CheckPII analyzer')
    checkpii_path = os.path.join(_project_root(), 'bin', 'CheckPII.jar')
    if not os.path.exists(checkpii_path):
        err = 'Could not find the CheckPII.jar analyzer!'
        _debug(err)
        raise RuntimeError(err)

    _debug('Check-Requirements complete!\r\n')


def _build_classifier():
    """
    Build a MALLET classifier from the examples given in the script's "training_set_path"

    :return: A sequence of all class labels discovered in the training set.
    """

    _debug('Build-Classifier is building a classifier from existing data...')

    full_mallet_path = os.path.join(_project_root(), mallet_path)
    import_path = os.path.join(_output_path(), 'data.mallet')
    _debug(f' -- Importing {import_path}\* into Mallet')

    # call the Mallet executable (relative path defined above) and its import-dir method.
    # paths are absolute, except for the input - relative makes for prettier label names.
    import_args = [full_mallet_path, 'import-dir',
                   '--input', os.path.abspath(os.path.join(training_set_path, '*')),
                   '--output', import_path]

    _debug(f' -- {" ".join(import_args)}')

    # redirect stderr (Java, ugh) to stdout, and open a file to receive stdout
    # this is running as shell, so shlex.quote() if the args array comes from the public
    with open(os.path.join(_output_path(), 'import.log'), 'w') as log:
        subprocess.run(import_args, shell=True,
                       stderr=subprocess.STDOUT,
                       stdout=log)
    _debug(' -- Importing complete.')

    # make sure that we wrote the thing we thought we wrote, because we'll need it later.
    _debug(f' -- Searching for imported data: "{import_path}"')
    if not os.path.exists(import_path):
        raise RuntimeError('importing data into Mallet did not produce an index file as expected!')

    _debug(f' -- Training {classifier_type} classifier on indexed data...')

    classifier = os.path.join(_output_path(), classifier_name)
    training_args = [full_mallet_path, 'train-classifier',
                     '--input', import_path,
                     '--output-classifier', classifier,
                     '--trainer', classifier_type]

    if cross_validation:
        _debug(f' -- cross-validation specified: "{cross_validation}"')
        training_args.append('--cross-validation')
        training_args.append(cross_validation)

    _debug(f' -- {" ".join(training_args)}')

    # redirect stderr (Java, ugh) to stdout, and open a file to receive stdout
    # this is running as shell, so shlex.quote() if the args array comes from the public
    with open(os.path.join(_output_path(), 'training.log'), 'w') as log:
        subprocess.run(training_args, shell=True,
                       stderr=subprocess.STDOUT,
                       stdout=log)

    # Save the labels Mallet discovered to a list. To do this:
    # open the file that Mallet wrote to stderr (but we redirected to stderr)
    # Skip the first line because it's a header
    # Split that string on path separator in case multiple folder levels were imported
    # Take the last of those splits (final directory) and strip any whitespace \ newlines
    with open(os.path.join(_output_path(), 'import.log'), 'r') as log:
        full_labels = log.readlines()[1:]
        labels = [l.split('\\')[-1].rstrip() for l in full_labels]

    _debug('Build-Classifier complete!\r\n')

    return labels


def _classify_file(file_path, labels):
    """
    Classify a text file as belonging to one of the classes discovered by build_classifier().

    :param file_path: The path to the text file you wish to score.
    :param labels: A sequence of all class labels discovered in the training set.

    :return: A dictionary containing two objects. One, having the key 'classification',
    indicates which of the classifier's discovered labels applies to the document. The
    other, named 'confidence', shows the scores determined for each class.
    """

    _debug(f'Classify-Document called for file {file_path}')

    classify_input_path = os.path.join(_output_path(), 'classified_docs', 'input.txt')
    _debug(f' -- Copying {file_path} to {classify_input_path}')

    if not os.path.exists(os.path.dirname(classify_input_path)):
        os.makedirs(os.path.dirname(classify_input_path))

    # classify-dir scores the file as a whole, whereas classify-file scores each line separately.
    # since we want to score a whole file, we need to move it into its own directory.
    shutil.copyfile(file_path, classify_input_path)

    # setup executable, input, and output paths separately to keep args list readable
    classifier_path = os.path.join(_output_path(), classifier_name)
    classifier_output_path = os.path.join(_output_path(), 'classified.txt')
    full_mallet_path = os.path.join(_project_root(), mallet_path)

    classify_args = [full_mallet_path, 'classify-dir',
                     '--input', os.path.dirname(classify_input_path),
                     '--output', classifier_output_path,
                     '--classifier', classifier_path]

    _debug(f' -- Classifying document with classifier {classifier_name}')
    _debug(' -- ' + ' '.join(classify_args))

    # redirect stderr (Java, ugh) to stdout, and open a file to receive stdout
    # this is running as shell, so shlex.quote() if the args array comes from the public
    with open(os.path.join(_output_path(), 'classify.log'), 'w') as log:
        subprocess.run(classify_args, shell=True,
                       stderr=subprocess.STDOUT,
                       stdout=log)

    # two tokens per class (name and score), but negative so it slices from the end
    number_of_class_tokens = len(labels) * -2
    with open(classifier_output_path, 'r') as out:
        classes_and_scores = out.read().split()[number_of_class_tokens:]

    highest_score = 0
    highest_label = ''
    all_classes = {}
    for i in range(0, len(classes_and_scores), 2):
        label = classes_and_scores[i]
        score = classes_and_scores[i + 1]

        all_classes[label] = score

        if float(score) > highest_score:
            highest_score = float(score)
            highest_label = label

    _debug('Classify-Document complete!\r\n')
    return {
        'classification': highest_label,
        'confidence': all_classes
    }


def _check_file_for_pii(file_path):
    """
    Run a java analyzer against the text to find any Personally Identifiable Information.

    :param file_path: The path to the text file you wish to score.
    :return: A string, indicating whether PII was found.
    """

    _debug(f'Check-PII called for file {file_path}')

    jar_path = os.path.join(_project_root(), 'bin', 'CheckPII.jar')

    checkpii_args = ['java', '-jar', jar_path, file_path]
    _debug(' -- ' + ' '.join(checkpii_args))

    # this time we're stdout-ing to a pipe, which lands in the python variable.
    # this is running as shell, so shlex.quote() if the args array comes from the public
    with open(os.path.join(_output_path(), 'checkpii.log'), 'w') as log:
        checkpii_result = subprocess.run(checkpii_args, shell=True,
                                         stderr=log,
                                         stdout=subprocess.PIPE)

    str_result = checkpii_result.stdout.decode("utf-8").rstrip()
    _debug(' -- ' + str_result)

    _debug('Check-PII complete!\r\n')
    return str_result


def _module_path():
    """
    Return the directory of the script or compiled executable in which this module resides.

    :return: String representing the path of this module
    """

    if hasattr(sys, "frozen"):  # All of the modules are built-in to the interpreter, e.g., by py2exe
        _debug('module_path is returning "sys.frozen" path from sys.executable...')
        return os.path.dirname(sys.executable)
    return os.path.realpath(os.path.dirname(__file__))


def _project_root():
    """
    Return the root directory of the project, even if executing code is in a subfolder.

    :return: String representing the root directory of the project.
    """

    # the code lives in a 'src' folder, so the root will be one up from there.
    return os.path.abspath(os.path.join(_module_path(), os.pardir))


def _output_path():
    """Get the directory to which we should write output.

    Output will go to (project root)\output\(session_id). If no session_id is set,
    this will be a string representing the timestamp at which execution started, so
    all results are archived across executions. If you want to reuse the same folder
    every time, you can set session_id to be a string of your own choosing.

    This method does not verify that session_id is valid for use in filesystem paths.

    :return: String representing a file path, guaranteed to exist.
    """

    out_path = os.path.join(_project_root(), 'output', session_id)
    if not os.path.exists(out_path):
        os.makedirs(out_path)  # could throw OSError if created between the last line & now
    return out_path


def _debug(string):
    """
    Write the input string to stdout, as well as (output directory)\scorer.log

    :return: None. Side effects (e.g., to stderr or files) will be determined by the
    configuration of the logger created for the module.
    """
    if not is_debug:
        return

    logger = logging.getLogger()
    logger.debug(string)


# main entry point to script
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: {} path/file_to_score.txt".format(sys.argv[0]))
        sys.exit(1)

    path = sys.argv[1]
    if not os.path.isabs(path):
        _debug(f'requested file "{path}" is not an absolute path. Trying "{os.path.abspath(path)}"')
        path = os.path.abspath(path)

    # if there were more configurable options, I'd pipe command input to args and kwargs...
    if len(sys.argv) > 2:
        session_id = sys.argv[2]

    result = score_document(path)
