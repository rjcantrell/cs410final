import java.util.*;
import java.util.regex.*;
public class SSNDetector {
	String line;
	// Specify the regular expression to match SSN
	Pattern ptr = Pattern.compile("^\\d{3}[- ]?\\d{2}[- ]?\\d{4}$");
	public SSNDetector(String line) {
		this.line=line;
	}
	
	/**
	 * 
	 * @return true if the input line has SSN, false otherwise
	 */
	public boolean detectSSN() {
		Matcher m = ptr.matcher(line);
		while(m.find()) {
            return true;   
        }
        return false;
	}
	
	
}
