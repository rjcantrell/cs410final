import java.io.BufferedReader;
import java.io.FileReader;
/**
 * 
 * *
 * @author Sriram Dandapani, RJ Cantrell, Biswadeep Sarkar
 * CheckPII is the main class that does the following:
 * 1. Examines a text file line by line
 * 2. For each line, it checks for presence of an email,full name or Social Security Number
 * 3. It prints out the fact that PII (Personal Identifiable Information) was present or not
 *
 */
public class CheckPII {
	public static void main(String[] args) {
    	//The first argument is the token count(2 for bigram, 3 for trigram)
    	//The second argument is the file name
    	BufferedReader br=null;
    	String line="";
    	int i=0;
    	boolean hasPII=false;
    	String fileName=args[0];
    	try {
    		br = new BufferedReader(new FileReader(fileName));
    		while((line=br.readLine())!= null) {
    			i++;
		    	NgramTokenizer nt = new NgramTokenizer(2,line);
		    	while (nt.hasNext()) {
		    		String currentToken = nt.next();
		    	//	System.out.println(currentToken);
		    		//Check if the current token (which should be 2 words) follows the capitalized scheme.
		    		String[] fullName = currentToken.split(" ");
		    		if(fullName != null && fullName.length==2 && fullName[0] !=null && fullName[1] != null
		    				&& fullName[0].length() > 0 && fullName[1].length() > 0) {
		    			if (Character.isUpperCase(fullName[0].charAt(0)) && 
			    				Character.isUpperCase(fullName[1].charAt(0))) {
		    				if(!isWordUpperCase(fullName[0]) && !isWordUpperCase(fullName[1])) {
				    			System.err.println("Name PII_DETECTED in line " + line);
				    			hasPII=true;
		    				}
			    			
			    		}
		    		}
		    	}
		    	//Now check for Email
		    	EmailDetector dt = new EmailDetector(line);
		    	if (dt.detectEmail()) {
		    		System.err.println("Email PII_DETECTED in line " + line);
		    		hasPII=true;
		    	}
		    	NgramTokenizer ntSSN = new NgramTokenizer(1,line);
		    	int j=0;
		    	while (ntSSN.hasNext()) {
		    		j++;
		    		String currentToken = ntSSN.next();
		    		//Strip out SSN:(present in data files)
		    		currentToken = currentToken.replace("SSN:","");
		    		currentToken = currentToken.replace("\"", "");
		    		SSNDetector ssn = new SSNDetector(currentToken);
			    	if (ssn.detectSSN()) {
			    		System.err.println("SSN PII_DETECTED in line " + line);
			    		hasPII=true;
			    	}
//			    	System.out.println("Token " + j);
		    	}
//		    System.out.println("Reading line " + i);
    		}
  
    		if(hasPII) {
    			System.out.println("PII_DETECTED in file " + fileName);
    			System.exit(1);
    		}
    		else {
    			System.out.println("NO_PII_DETECTED in file " + fileName);
    			System.exit(0);
    		}
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    }
	
	/**
	 * 
	 * @param word
	 * @return boolean
	 * isWordUpperCase checks if a word is fully uppercase and returns a boolean indicating true or false
	 */
	private static boolean isWordUpperCase(String word){
        
       char[] letterArray = word.toCharArray();
       for(int i=0; i < letterArray.length; i++){
            //if the character is a letter
            if( Character.isLetter(letterArray[i]) ){
                //if any character is not in upper case, return false
                if( !Character.isUpperCase( letterArray[i] ))
                    return false;
            }
        }
        
        return true;
    }

}
