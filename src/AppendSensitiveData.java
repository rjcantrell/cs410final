import java.io.*;

import java.util.Random;


/**
 * This class introduces PII(Personal Identifiable information) into log files
 * This is just a helper class..it is a fully funcational PII injector, but provides
 * the structure to do so.
 */
public class AppendSensitiveData {
	
	public static void main(String[] args) {
		BufferedWriter bw=null;
		BufferedReader br=null;
		try {
			bw = new BufferedWriter(new FileWriter(args[1]));
			String line="";
			int i=0;
			String startSocial="080-10-";
			Random rand = new Random();
			String startEmail="sr.dan";
			br = new BufferedReader(new FileReader(args[0]));
			//For ever X lines, add sensitive data
			while((line=br.readLine())!=null) {
				int rndInt = rand.nextInt(10000);
				String outLine= line + "\"Email:" + startEmail + rndInt + "@illinois.edu\"";
				bw.write(outLine);
				bw.newLine();
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bw.close();
				br.close();
			} catch(Exception fe) {
				
			}
		}
		
	}
}
