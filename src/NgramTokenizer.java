import java.util.*;
import java.io.*;

/**
 * 
 * Tokenizes a string and returns an iterator that can be used to iterate over the tokens
 *
 */
public class NgramTokenizer implements Iterator<String> {

    String[] words;
    int pos = 0, n;

    /**
     * 
     * @param n  the n in the n-gram (1,2 ,3 etc)
     * @param str  the string to tokenize
     */
    public NgramTokenizer(int n, String str) {
        this.n = n;
        words = str.split(" ");
    }

    /**
     * @return true if the iterator has more tokens, false otherwise
     */
    public boolean hasNext() {
        return pos < words.length - n + 1;
    }

	/**
	 * @return the next token
	 */
    public String next() {
        StringBuilder sb = new StringBuilder();
        for (int i = pos; i < pos + n; i++)
            sb.append((i > pos ? " " : "") + words[i]);
        pos++;
        return sb.toString();
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
    
    
}
