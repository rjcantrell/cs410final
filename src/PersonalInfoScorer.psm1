﻿$script:outputPath = ''
$script:classifierName = 'CS410Final'
$script:classifierLabels = @()
$malletPath = "..\bin\mallet\bin\mallet"
$trainingSetPath = "..\data"

Function Score-Document {
    Param([string]$Filename=$defaultInFile)

    Write-Debug "Score-Document called for file $Filename"

    Set-Location $PSScriptRoot
    Check-Requirements $Filename #doesn't need to return success boolean, as it throws any errors it finds
    Build-Classifier #should this take an output file path as a param so other commands can use it?

    $results = @{} #this hashtable will contain a key for each analysis done, with its result as value

    $classificationResults = $(Classify-Document -Filename $Filename)

    $results += $classificationResults
    $results['check_pii'] = Check-Pii $Filename
    
    Write-Debug 'Results for document scoring:'
    $results |% { Write-Debug $($_ | Out-String) }
    
    Check-Pii $Filename

    return $results
} #end of PersonalInfoScorer.ScoreDocument($filename)
Export-ModuleMember -Function Score-Document



Function Check-Requirements {
    Param([string]$Filename)
    
    Write-Debug "Check-Requirements is verifying necessary components..."
    Write-Debug " -- Looking for $Filename"
    
    if(-not(Test-Path $Filename)) {
        Throw "Input file $Filename not found!"
    }

    $folders = $malletPath, $trainingSetPath
    $folders |% {
        Write-Debug " -- Looking for the folder $_, relative to $(Get-Location)"
        if (-not(Test-Path $_)) { Throw "Could not find the necessary $_ folder!" }
    }

    #these are the two labels the classifier assigns, so these are the two folders we expect
    $dataDirs = $(Join-Path $trainingSetPath 'sensitive'), $(Join-Path $trainingSetPath 'nonsensitive')
    $dataDirs |% {
        Write-Debug " -- Looking for the data folder $_"
        if (-not(Test-Path $_ -PathType Container)) { 
            Throw "Expected $_ to exist!" 
        }

        Write-Debug " -- Checking whether $_ is empty"
        if (-not(Get-ChildItem $_ -File)) {
            Throw "$_ exists but contains no files for training a classifier!"
        }
    }

    Write-Debug 'Check-Requirements complete!'
} #end of Check-Requirements
#Export-ModuleMember -Function Check-Requirements



Function Build-Classifier {
    Write-Debug 'Build-Classifier is building a classifier from existing data...'

    #index your training data -- folder names are used as labels, with each file being an observation
    $indexPath = Join-Path $(Get-OutputDirectory) "data.mallet" # relative to the execution root
    $importArgs = ("import-dir", "--input", "$trainingSetPath\*", "--output", "$indexPath")

    Write-Debug " -- Importing $trainingSetPath\* into Mallet..."    
    $importOutputPath = Join-Path $(Get-OutputDirectory) 'import.log'
    Write-Debug " -- $malletPath $importArgs"

    Write-ToFile -command "$malletPath $importArgs" -outFile $importOutputPath
    Write-Debug " -- Importing complete."

    #I'm not sure why I can't get the program's output on the console...
    if(-not(Test-Path $indexPath)) {
        Throw 'importing data into Mallet did not produce an index file as expected!'
    }

    Get-Content -Path $importOutputPath | 
        Select-Object -Skip 1 |
        % {
            $splits = $_ -Split "\\"
            $script:classifierLabels += [string]$splits[$splits.Length - 1]
        }

    $classifierType = 'MaxEnt'
    $crossValidation = '' #'--cross-validation 10' #"--cross-validation 10" to do CV, or " " to skip
    $classifier = Join-Path $(Get-OutputDirectory) $script:classifierName
    $trainingArgs = ('train-classifier', "--input $indexPath", "--output-classifier $classifier",
    "--trainer $classifierType", $crossValidation)

    Write-Debug ' -- Training $classifierType classifier on indexed data...'
    #Start-Process $malletPath -ArgumentList $trainingArgs -Wait -NoNewWindow 

    $trainingOutputPath = Join-Path $(Get-OutputDirectory) 'training.log'
    Write-Debug " -- $malletPath $trainingArgs"
    Write-ToFile -command "$malletPath $trainingArgs" -outFile $trainingOutputPath
    Write-Debug 'Build-Classifier complete!'

} #end of Build-Classifier
#Export-ModuleMember -Function Build-Classifier



Function Classify-Document {
    Param([string]$Filename)

    Write-Debug "Classify-Document called for file $Filename"

    $fullFilenamePath = Join-Path $PSScriptRoot $Filename
    $classifyInputDir = [io.path]::combine($(Get-OutputDirectory), 'classified_docs') 
    Write-Debug "Copying $Filename to $classifyInputPath"
    
    #This line gets the file as a string array, then joins it into a single string, then writes to a new file
    #We have to save this to a variable because any created value not stored as a variable is returned...
    $_ = New-Item -ItemType Directory -Force -Path $classifyInputDir
    [string](Get-Content -Path $fullFilenamePath) | Out-File -Encoding "UTF8" $(Join-Path $classifyInputDir 'input.txt')

    #bin/mallet classify-file --input data --output - --classifier classifier
    $classifier = Join-Path $(Get-OutputDirectory) $script:classifierName
    $classifyOutputPath = Join-Path $(Get-OutputDirectory) 'classified.txt'
    $classifyLogPath = Join-Path $(Get-OutputDirectory) 'classify.log'

    #we don't need to specify the file encoding because the default is UTF8 (which is how we saved it)
    $classifyArgs = ('classify-dir', "--input $classifyInputDir", "--output $classifyOutputPath", "--classifier $classifier")
    Write-ToFile -command "$malletPath $classifyArgs" -outFile $classifyLogPath

    #to get the class assigned to the document, open the classifier's output, and take the last X words.
    #X is twice the number of classes the classifier distinguishes between (because each has a name and score)
    #you should count from the front of the array, not the back, so they are not in reverse order
    $classifiedResult = (Get-Content -Path $classifyOutputPath) -split '\s+'
    $numberOfTokens = $script:classifierLabels.Length * 2
    $tokens = $classifiedResult[($classifiedResult.Length - $numberOfTokens)..($classifiedResult.Length)]

    $allClasses = @{}
    $highestValue = 0.0
    $highestLabel = ''
    for ($i = 0; $i -lt $tokens.Length; $i = $i + 2)
    {
        $key = $tokens[$i]; $value = $tokens[($i + 1)]
        $allClasses[$key] = $value
        if ($value -gt $highestValue) { $highestValue = $value; $highestLabel = $key }
    }

    Write-Debug 'Classify-Document complete!'
    return @{
        'classification'=$highestLabel;
        'confidence'=$allClasses
    }
} #end of Classify-Document
#Export-ModuleMember -Function Classify-Document


Function Check-Pii {
    Param([string]$Filename)

    Write-Debug "Check-Pii called for file $Filename"

    #$relPath = Join-Path ()
    $checkPiiJarFile = Join-Path $PSScriptRoot '..\bin\CheckPII.jar'
    $checkPiiOutfile = Join-Path $(Get-OutputDirectory) 'checkpii.txt'
    Write-Debug " -- Executing 'java -jar $checkPiiJarFile $Filename'"
    #Write-ToFile -Command "java -jar $checkPiiJarFile $Filename" -outFile $findPiiOutfile
    $checkPiiResult = &java -jar $checkPiiJarFile $Filename > $checkPiiOutfile

    $result = Get-Content -Path $checkPiiOutfile
    Write-Debug " -- '$result'"
    Write-Debug "Check-Pii complete!"
    return $result
} #end of Find-Pii
#Export-ModuleMember -Function Find-Pii


Function Get-OutputDirectory {
    <#
    .SYNOPSIS 
    Tells the scorer the folder in which to drop any output it may create during execution.

    .DESCRIPTION
    Returns a folder path, creating it if it did not exist. 

    .INPUTS
    None. It takes no parametere, and you cannot pipe objects to Add-Extension.

    .OUTPUTS
    System.String. Get-OutputDirectory returns a string representing a path on disk. 
    The folder will be created if it did not previously exist.

    .LINK
    Get-OutputDirectory
    #>
    if(-not($script:outputPath)) {
        $_executionKey = Get-Date -Format 'yyyy_MM_dd_HH_mm_ss_ffff'
    
        $script:outputPath = $PSScriptRoot
        @('..', 'output', $_executionKey) | %{ $script:outputPath = Join-Path $outputPath $_ }
    }

    #if the directory doesn't exist, let's create it
    if (-not(Test-Path $script:outputPath)) {
        $_ = New-Item -ItemType Directory -Path $script:outputPath #assign to a var so it's not returned
    }

    return $script:outputPath
}
#Export-ModuleMember -Function Get-OutputDirectory #we don't need to call it. Score-Document is public entry point.



Function Write-ToFile {
    <#
    .SYNOPSIS 
    Syntactic sugar in CmdLet form. Runs a certain process from the command line, 
    redirecting both the standard output standard error streams to a single text file.

    .DESCRIPTION
    Call it when you don't want to memorize the 2>&1 thingamajig.

    .INPUTS
    None. It takes no parametere, and you cannot pipe objects to Add-Extension.

    .OUTPUTS
    None that can be piped within Powershell. A side effect is that the specified file is created.

    .LINK
    Write-ToFile
    #>
    [CmdletBinding()]
    Param([string]$command, [string]$outFile)
    $result = cmd /c "$command `>$outFile 2`>`&1"
}
