@ECHO OFF

REM joinpath below takes two string arguments. If the first ends with a backslash, they're concatenated.
REM if the first does not end in a backslash, it is concatenated with a backslash and then the second value
CALL :joinpath %~dp0 "bin\mallet"
SET MALLET_PATH=%Result%

REM Mallet requires an environment variable, but maybe you're already using Mallet on your own.
REM Make sure that we respect the existing setting by changing it back to your chosen value after execution.
IF NOT "%MALLET_HOME%" == "%MALLET_PATH%" (
	IF DEFINED MALLET_HOME (
		ECHO MALLET_HOME exists as '%MALLET_HOME%' -- temporarily resetting as '%MALLET_PATH%'
		SET OLD_MALLET_HOME=%MALLET_HOME%
	)
	SET MALLET_HOME=%MALLET_PATH%
)

REM I mean, if you really want to. I feel dirty for having written this.
REM powershell -command .\src\runme.ps1 %1

REM This is the script that actually does the work of importing data, training a classifier and classifying the document.
python .\src\scorer.py %*

REM Give the user their old Kentucky home -- I mean, their previous MALLET_HOME -- now that we're done.
IF NOT "%OLD_MALLET_HOME%" == "" (
	ECHO resetting MALLET_HOME to its previous value of '%OLD_MALLET_HOME%'
)
SET MALLET_HOME=%OLD_MALLET_HOME%

REM best part of any BAT file.
GOTO :eof

REM joinpath below takes two string arguments. If the first ends with a backslash, they're concatenated.
REM if the first does not end in a backslash, it is concatenated with a backslash and then the second value
:joinpath
SET Path1=%~1
SET Path2=%~2
IF {%Path1:~-1,1%}=={\} (set Result=%Path1%%Path2%) else (set Result=%Path1%\%Path2%)

GOTO :eof