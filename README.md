﻿# CS410Final


## Overview
Part 1:
We have made an extensive research on publicly available tools for Data Mining practices and after meticulously considering several available options, chose MALLET (Machine Learning for Language Toolkit) from University of Massachusetts, Amherst as our staring tool to custom build our API for the project. MALLET is a Java-based package which provides sophisticated tools for document classification along with many other inbuilt features. Further information on Mallet can be found at http://mallet.cs.umass.edu/

Part 2:
As in the world of document classification, the biggest responsibility lies on building the correct corpus to train any given classifier (Naïve Bayes, MaxEnt, DecisionTree etc.), we have built a program in Java to come up with creating significant volume of training and testing documents ensuring rightly mixed and balanced sets of sensitive documents pertaining to different types of PII information on which we can train different classifiers to achieve our objectives.

Part 3:
On top of the existing tokenization algorithm provided by Mallet, we have created a Ngram Tokenizer in Java to classify the input documents in sensitive/Non-sensitive category based on collection of words. This is required because sensitive data cannot be always identified based on the presence of a word (For example the word ‘SSN’ itself does not make a document sensitive unless combined with a valid 9 digits SSN number).
Part 4:
Finally we have the complete wrapper API built in Python to invoke the necessary MALLET commands and perform the tasks:
Import the directories and create labels for Sensitive and Non-Sensitive data based on a set of input documents.
Specify the required classifier (MaxEnt, NaiveBayes, DecisionTree) along with parameters (train/test split, iterations/trials etc.).
Train the classifier based on set of input documents.
Classify a test document and get the sensitivity score.
1. As an atomic output – whether the document is sensitive or not.
2. Extent of Sensitivity – Sensitive vs Non-Sensitive percentage output.


## Usage
Mallet is a Java based Classification framework/tool built by the University of Massachusetts. The software offers classification algorithms that can be invoked via the command line or the Java API.
One of the goals of this project was to see if a classification-based approach to identification of sensitive information was practical and useful for specific types of data.  Of specific interest was the ability to determine presence of sensitive data in log files.  The specific use case targeted was whether a new log file that was to be tested for sensitive information belonged to a specific label identified by the classifier.
We downloaded police incident log files from data.world. The single log file was 5MB in size. Following was done to train the Mallet Classifier:
https://data.world/us-arlington-county/f960c5d2-fb42-42e6-a36d-6cc0704fc23b/workspace/file?filename=comma-separated-values-file-2.csv
 
1. Split the log file into 500KB log files. This was done as typical log files vary in size from organization to organization. They get rotated when they reach a certain size. We could have gone with smaller size, but settled on 500KB
2. The log files did not have any personal identifiable information. 
3. The log files were placed in a folder called “nonsensitive”
4. A new folder called “sensitive” was created. Both folders (sensitive and nonsensitve) were placed in a directory called “logs”
5. A java program was written to inject Personal Information in each line for every file read from the nonsensitive. The personal information was randomly generated( for each line). The modified lines were then output to corresponding files in the “sensitive” folder. This is how we obtained the training set for the sensitive information. 
6. A combination of Email and Social Security Numbers were sprinkled across all the files that were generated. Every line had Personal Information. This was done to mimic typical applications that have code that write Personal Information to files. (every log will typical have the information)
7. Mallet was used to run the test for classification. The process with Mallet is described in the Run Manual Section:

Run Manual

Pre-Requisites

1. Python 3.6 is required. This can be downloaded from 
https://www.python.org/downloads/release/python-360/
The system PATH variable needs to point to the python36 directory that was installed.
If using Windows, please add the python36 directory to the PATH variable before running command prompt.

2. The latest java runtime 1.8.X is required
Typing java --version should show below output
java version "1.8.0_121"
Java(TM) SE Runtime Environment (build 1.8.0_121-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)



Step 1: Download the repository from the GitHub.
URL (Needs to be changed)
https://bitbucket.org/rjcantrell/cs410final

Step 2: Test a file for sensitivity:
a.	Test a sensitive file
	Navigate to the folder that is the root of the repository
	Run the below command: (From Command Prompt in Administrator mode)
	runme <full-path-of-repository-root>\sample-sensitive.txt 
      b. Test a non-sensitive file
	Navigate to the folder that is the root of the repository
	Run the below command:
	runme <full-path-of-repository-root>\sample-nonsensitive.txt 

Note: please use forward slashes for a linux machine. (the code was tested on windows and MAC)

The output will indicate the analysis of Mallet on the file as well as the custom java code that is fingerprinting the data for exact PII matches.

## Acknowledgements

